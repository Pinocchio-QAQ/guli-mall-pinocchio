package icu.pinocchio.gulimall.thirdpart.oss;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/27 3:43
 */
@SpringBootTest
public class TestOss {

    @Resource
    private OSS ossClient;

    @Test
    public void testUpload(){
        try {
            InputStream inputStream = new FileInputStream("E:\\图片\\杏\\杏.jpg");
            PutObjectRequest putObjectRequest = new PutObjectRequest("gulimall-pinocchio", "杏.jpg", inputStream);
            ossClient.putObject(putObjectRequest);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally{
            ossClient.shutdown();
        }
        System.out.println("上传完成.");


    }
}
