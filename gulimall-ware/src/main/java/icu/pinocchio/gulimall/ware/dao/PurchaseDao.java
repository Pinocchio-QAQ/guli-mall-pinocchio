package icu.pinocchio.gulimall.ware.dao;

import icu.pinocchio.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:33:55
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
