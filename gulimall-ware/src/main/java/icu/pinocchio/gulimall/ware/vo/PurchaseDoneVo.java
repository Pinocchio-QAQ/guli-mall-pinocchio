package icu.pinocchio.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/3/1 23:56
 */
@Data
public class PurchaseDoneVo {
    private Long id;
    private List<PurchaseItemDoneVo> items;
}
