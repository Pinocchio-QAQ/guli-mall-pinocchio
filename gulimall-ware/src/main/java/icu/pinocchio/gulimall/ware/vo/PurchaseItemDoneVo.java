package icu.pinocchio.gulimall.ware.vo;

import lombok.Data;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/3/1 23:55
 */
@Data
public class PurchaseItemDoneVo {
    private Long itemId;
    private Integer status;
    private String reason;
}
