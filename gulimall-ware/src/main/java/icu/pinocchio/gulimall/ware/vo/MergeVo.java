package icu.pinocchio.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/3/1 19:24
 */
@Data
public class MergeVo {
    /**
     * 采购单id
     */
    private Long purchaseId;
    /**
     * 合并采购需求id列表
     */
    private List<Long> items;
}
