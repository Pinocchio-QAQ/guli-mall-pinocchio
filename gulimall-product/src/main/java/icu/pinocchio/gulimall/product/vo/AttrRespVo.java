package icu.pinocchio.gulimall.product.vo;

import icu.pinocchio.gulimall.product.entity.AttrEntity;
import lombok.Data;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/28 3:37
 */
@Data
public class AttrRespVo extends AttrEntity {
    /**
     * 所属分类名称
     */
    private String catelogName;
    /**
     * 所属分组名称
     */
    private String groupName;

    /**
     * 分类路径
     */
    private Long[] catelogPath;
}
