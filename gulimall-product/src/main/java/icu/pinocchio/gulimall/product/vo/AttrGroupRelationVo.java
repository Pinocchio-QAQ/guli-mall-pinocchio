package icu.pinocchio.gulimall.product.vo;

import lombok.Data;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/28 6:05
 */
@Data
public class AttrGroupRelationVo {

    private Long attrId;

    private Long attrGroupId;
}
