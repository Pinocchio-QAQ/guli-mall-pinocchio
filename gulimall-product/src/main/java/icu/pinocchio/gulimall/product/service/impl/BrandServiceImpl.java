package icu.pinocchio.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import icu.pinocchio.gulimall.product.dao.CategoryBrandRelationDao;
import icu.pinocchio.gulimall.product.entity.CategoryBrandRelationEntity;
import icu.pinocchio.gulimall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.common.utils.Query;

import icu.pinocchio.gulimall.product.dao.BrandDao;
import icu.pinocchio.gulimall.product.entity.BrandEntity;
import icu.pinocchio.gulimall.product.service.BrandService;
import org.springframework.util.StringUtils;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    // @Autowired
    // private CategoryBrandRelationService relationService;

    @Autowired
    private CategoryBrandRelationDao relationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        // 1.支持模糊匹配key
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(key)){
            wrapper.like("brand_id",key).or().like("name",key);
        }

        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                wrapper
        );
        System.out.println("hot debug.");
        return new PageUtils(page);
    }

    @Override
    public void updateCascade(BrandEntity brand) {
        this.updateById(brand);
        // 级联更新冗余字段
        if (!StringUtils.isEmpty(brand.getName())){
            CategoryBrandRelationEntity entity = new CategoryBrandRelationEntity();
            entity.setBrandId(brand.getBrandId());
            entity.setBrandName(brand.getName());

            relationDao.update(entity,new UpdateWrapper<CategoryBrandRelationEntity>().eq("brand_id",brand.getBrandId()));
            // relationService.updateBrand(brand.getBrandId(),brand.getName());
        }

    }

}