package icu.pinocchio.gulimall.product.service.impl;

import com.sun.xml.internal.bind.v2.TODO;
import icu.pinocchio.gulimall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.common.utils.Query;

import icu.pinocchio.gulimall.product.dao.CategoryDao;
import icu.pinocchio.gulimall.product.entity.CategoryEntity;
import icu.pinocchio.gulimall.product.service.CategoryService;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService relationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 获取所有子菜单列表，并以树形结构组织返回
     * @return 所有菜单列表
     */
    @Override
    public List<CategoryEntity> listWithTree() {

        // 1.获取所有菜单
        List<CategoryEntity> categoryList = baseMapper.selectList(null);
        // 2.组织树形结构返回
        List<CategoryEntity> result = categoryList.stream()
                .filter(category -> {
                    return category.getParentCid() == 0;
                }).map(category -> {
                    List<CategoryEntity> categoryChildren = getCategoryChildren(category, categoryList);
                    category.setChildren(categoryChildren);
                    return category;
                }).collect(Collectors.toList());

        return result;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {

        // TODO:先判断菜单是否有其他关联才能删除
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        paths = findParentPath(catelogId,paths);

        // 逆序路径
        Collections.reverse(paths);

        return paths.toArray(new Long[paths.size()]);
    }

    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        // 级联更新冗余字段
        if (!StringUtils.isEmpty(category.getName())){
            relationService.updateCatogory(category.getCatId(),category.getName());
        }
    }

    /**
     * 递归查找菜单完整路径
     * @param catelogId 当前菜单id
     * @param paths 完整路径
     * @return
     */
    private List<Long> findParentPath(Long catelogId,List<Long> paths){
        // 1.收集当前菜单id
        paths.add(catelogId);
        // 2.获取父级菜单id
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid() != 0){
            findParentPath(byId.getParentCid(),paths);
        }
        return paths;
    }

    /**
     * 获取子菜单列表
     *
     * @param parentCategory 父菜单
     * @param allCategory    所有菜单列表
     * @return 子菜单列表
     */
    private List<CategoryEntity> getCategoryChildren(CategoryEntity parentCategory, List<CategoryEntity> allCategory) {
        List<CategoryEntity> children = allCategory.stream()
                .filter(category -> {
                    return category.getParentCid() == parentCategory.getCatId();
                }).map(category -> {
                    // 1.找到子菜单
                    category.setChildren(getCategoryChildren(category, allCategory));
                    return category;
                }).sorted((category1, category2) -> {
                    return (category1.getSort() == null ? 0 : category1.getSort()) - (category2.getSort() == null ? 0 : category2.getSort());
                }).collect(Collectors.toList());
        return children;
    }

}