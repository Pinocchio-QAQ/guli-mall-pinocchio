package icu.pinocchio.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:06:26
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

