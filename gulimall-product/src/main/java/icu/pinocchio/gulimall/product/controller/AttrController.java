package icu.pinocchio.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import icu.pinocchio.gulimall.product.entity.ProductAttrValueEntity;
import icu.pinocchio.gulimall.product.service.ProductAttrValueService;
import icu.pinocchio.gulimall.product.vo.AttrRespVo;
import icu.pinocchio.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import icu.pinocchio.gulimall.product.entity.AttrEntity;
import icu.pinocchio.gulimall.product.service.AttrService;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.common.utils.R;



/**
 * 商品属性
 *
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-15 23:28:14
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    @PostMapping("/update/{spuId}")
    public R updateSpuAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> entities){
        productAttrValueService.updateSpuAttr(spuId,entities);
        return R.ok();
    }


    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrListForSpu(@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> entities = productAttrValueService.baseAttrlistforspu(spuId);
        return R.ok().put("data",entities);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
        public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/{attrType}/list/{categoryId}")
    public R listBaseAttr(@RequestParam Map<String, Object> params,@PathVariable Long categoryId,
                          @PathVariable String attrType){
        // PageUtils page = attrService.queryPage(params);
        PageUtils page = attrService.queryPageBaseAttr(params,categoryId,attrType);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId){
		// AttrEntity attr = attrService.getById(attrId);

        AttrRespVo respVo = attrService.getAttrInfo(attrId);

        return R.ok().put("attr", respVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrEntity attr){
		attrService.saveDetail(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrVo attrVo){
		// attrService.updateById(attr);

        attrService.updateAttr(attrVo);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
        public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
