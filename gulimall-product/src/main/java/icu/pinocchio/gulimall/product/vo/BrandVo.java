package icu.pinocchio.gulimall.product.vo;

import lombok.Data;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/28 14:43
 */
@Data
public class BrandVo {
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 品牌名称
     */
    private String brandName;
}
