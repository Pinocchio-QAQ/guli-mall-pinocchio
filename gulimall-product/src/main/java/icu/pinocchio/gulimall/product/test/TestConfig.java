package icu.pinocchio.gulimall.product.test;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Properties;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/17 18:02
 */
@RestController
@RequestMapping("product/test")
public class TestConfig {
    @Autowired
    private ApplicationContext context;

    @RequestMapping("student")
    public String getStudent() throws NacosException {

        Student student = context.getBean(Student.class);

        System.out.println(student);
        System.out.println(student.hashCode());
        return student.toString();
    }
}

@RefreshScope
@Component
@Scope("prototype")
class Student{

    @Value("${student.name}")
    private String name;
    @Value("${student.age}")
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
