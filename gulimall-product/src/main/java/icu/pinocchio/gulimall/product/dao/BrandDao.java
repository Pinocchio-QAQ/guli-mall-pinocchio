package icu.pinocchio.gulimall.product.dao;

import icu.pinocchio.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-15 23:10:56
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
