package icu.pinocchio.gulimall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import icu.pinocchio.gulimall.product.entity.BrandEntity;
import icu.pinocchio.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Test
    public void testInsert(){
        BrandEntity brand = new BrandEntity();
        brand.setName("小米");
        brandService.save(brand);
    }

    @Test
    public void testQuery(){

        List<BrandEntity> list = brandService.list();
        list.forEach(brand -> {
            System.out.println(brand);
        });
    }

}
