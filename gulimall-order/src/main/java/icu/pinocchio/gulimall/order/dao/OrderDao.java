package icu.pinocchio.gulimall.order.dao;

import icu.pinocchio.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:24:39
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
