package icu.pinocchio.gulimall.order.dao;

import icu.pinocchio.gulimall.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:24:39
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
