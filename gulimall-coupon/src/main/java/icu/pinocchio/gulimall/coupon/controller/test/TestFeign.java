package icu.pinocchio.gulimall.coupon.controller.test;

import icu.pinocchio.gulimall.common.utils.R;
import icu.pinocchio.gulimall.coupon.feign.ProductClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/17 0:05
 */
@RestController
@RequestMapping("coupon/feign")
public class TestFeign {

    @Autowired
    ProductClient productClient;

    @RequestMapping("/product")
    public R testProduct(){
        R result = productClient.list(new HashMap<>());
        return result;
    }
}
