package icu.pinocchio.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.coupon.entity.CouponHistoryEntity;

import java.util.Map;

/**
 * 优惠券领取历史记录
 *
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:28:31
 */
public interface CouponHistoryService extends IService<CouponHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

