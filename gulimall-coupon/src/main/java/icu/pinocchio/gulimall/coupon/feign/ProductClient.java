package icu.pinocchio.gulimall.coupon.feign;

import icu.pinocchio.gulimall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/17 0:01
 */
@FeignClient("gulimall-product")
public interface ProductClient {
    @RequestMapping("/product/category/list")
    public R list(@RequestParam(required = false) Map<String, Object> params);
}
