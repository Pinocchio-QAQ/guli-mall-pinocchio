package icu.pinocchio.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import icu.pinocchio.gulimall.common.utils.PageUtils;
import icu.pinocchio.gulimall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:28:31
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

