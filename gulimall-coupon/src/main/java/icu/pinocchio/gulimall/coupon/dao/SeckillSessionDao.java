package icu.pinocchio.gulimall.coupon.dao;

import icu.pinocchio.gulimall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 * 
 * @author Pinocchio
 * @email pinocchioqaq@gmail.com
 * @date 2023-02-16 00:28:31
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {
	
}
