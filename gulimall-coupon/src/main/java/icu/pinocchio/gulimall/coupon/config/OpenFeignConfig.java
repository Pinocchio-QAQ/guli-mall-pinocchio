package icu.pinocchio.gulimall.coupon.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Pinocchio
 * @version 1.0
 * @description TODO
 * @date 2023/2/17 14:01
 */
@Configuration
public class OpenFeignConfig {

    /**
     * OpenFeign日志级别定义
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }
}
